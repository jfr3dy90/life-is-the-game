using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class DanceController : MonoBehaviour
{
    [SerializeField] private Transform menuCamera;
    [SerializeField] private Transform targetCamera;
    [SerializeField] private float cameraSpeedRotation;
    
    [SerializeField] private Animator animCont_Dance;
    private float danceValue;

    private const string isDance = "IsDance";
    private const string dance = "Dance";
    private IEnumerator selectDance;

    private const float _timeToChange = 1f;
    private const float _tolerance = 0.01f;
    private const float _toleranceToChange = .49f;


    private void Start()
    {
        DontDestroyOnLoad(transform);
        ActionsController.OnDance += OnSetDance;
        ActionsController.OnSelectDance += OnSelectDance;
    }
    
    private void Update()
    {
        Vector3 direction = targetCamera.position - menuCamera.position;
        direction.y = 0;
        Quaternion desireAngle = quaternion.LookRotation(direction, Vector3.up);
        menuCamera.rotation = Quaternion.Slerp(menuCamera.rotation, desireAngle, cameraSpeedRotation * Time.deltaTime);
    }

    private void OnDestroy()
    {
        ActionsController.OnDance -= OnSetDance;
        ActionsController.OnSelectDance -= OnSelectDance;
    }

    private void OnSetDance(bool isDancing)
    {
        animCont_Dance.SetBool(isDance, isDancing);
    }

    private void OnSelectDance(float targetDance)
    {
        if (Math.Abs(targetDance - danceValue) > _toleranceToChange)
        {
            StarSelectDance(targetDance);
        }
    }

    private void StarSelectDance(float targetDance)
    {
        if (selectDance != null)
        {
            StopCoroutine(selectDance);
        }

        selectDance = SelectingDance(targetDance);
        StartCoroutine(selectDance);
    }

    private IEnumerator SelectingDance(float targetDance)
    {
        float speedChange = Mathf.Abs(targetDance - danceValue) / _timeToChange;
        while (Math.Abs(targetDance - danceValue) > _tolerance)
        {
            yield return new WaitForEndOfFrame();
            danceValue = Mathf.MoveTowards(danceValue, targetDance, speedChange * Time.deltaTime);
            animCont_Dance.SetFloat(dance, danceValue);
        }
    }
}