using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundFxController : MonoBehaviour
{
    public static SoundFxController Instance;
    [SerializeField] private AudioSource audioSource;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySoundFx(Vector3 desirePosition , AudioClip audioClip)
    {
        transform.position = desirePosition;
        audioSource.PlayOneShot(audioClip);
    }
}
