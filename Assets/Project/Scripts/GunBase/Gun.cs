using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public abstract class Gun : MonoBehaviour
{
    public bool gunReady = false;
    [SerializeField] protected int poolIndex;
    [SerializeField] protected Transform spawnPoint;
    [SerializeField] protected GameObject prefBullet;
    public GunData data;
    public int actualAmmo;
    private const int _zero = 0;

    protected virtual void Start()
    {
        actualAmmo = data.maxAmmoCount;
    }

    public abstract float ReloadTime();

    public virtual void Shoot()
    {
        if (actualAmmo > _zero)
        {
            SoundFxController.Instance.PlaySoundFx(transform.position, data.shoot_Sfx);
            actualAmmo--;
        }
    }

    public void ShootEmpty()
    {
       SoundFxController.Instance.PlaySoundFx(transform.position, data.empty_gun);
    }
}
