using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Gun", menuName = "Create/Gun")]

public class GunData : ScriptableObject
{
  #region Variables

  public string gunName;
  public int maxAmmoCount;
  public float reloadTime;
  public float damage;
  public float fireRate;
  public float range;
  public Vector2 recoil;
  public float returnSpeed;
  public float snappines;
  public Vector3 offsetPosition;

  public AudioClip shoot_Sfx;
  public AudioClip empty_gun;
  public bool automatic;


  #endregion
}
