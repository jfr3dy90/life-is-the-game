using UnityEngine;

public class GunController : MonoBehaviour
{
    #region Variables

    [SerializeField] private Transform camera;
    [SerializeField] private Transform gunPoint;

    [Header("Gun Controller")] [SerializeField]
    private Gun[] guns;

    [SerializeField] private Gun actualGun;
    private int indexGun;
    private const int _maxGuns = 3;

    [Header("Fire Gun")] [SerializeField] 
    private float lastShootTime = 0;

    [Header("Reload Gun")] private float lastReload;
    private bool reloadindg;

    [Header("Change Gun")] 
    [SerializeField] private float changeTime;

    private float lastChangeTime;
    private bool isChanging;

    [Header("Recoil")] 
    [SerializeField] private float defaultReturnSpeed;
    [SerializeField] private float returnSpeed;
    [SerializeField] private float snappines;
    
    private Vector3 currentRotation;
    private Vector3 targetRotation;
    

    [Header("Pick Gun")] [SerializeField] private LayerMask gunLayerMask;
    [SerializeField] private float distancePicKGun;
    [SerializeField] private float timePickGun;
    private float lastPickGun;
    private const float _dropForce = 2f;
    private const int _zero = 0;

    #endregion

    #region Unity Functions

    private void Start()
    {
        guns = new Gun[_maxGuns];
    }

    private void Update()
    {
        if (Physics.Raycast(camera.position, camera.forward, out RaycastHit hit, distancePicKGun, gunLayerMask))
        {
            if (hit.transform.GetComponent<Gun>() != null)
            {
                if (!UIManager.Instance.GetPickState())
                {
                    UIManager.Instance.SetHand(true, _zero);
                }
            }

            if (Input.GetButton("PickGun"))
            {
                lastPickGun += Time.deltaTime;
                if (lastPickGun >= timePickGun)
                {
                    PickGun(hit.transform.GetComponent<Gun>());
                    lastPickGun = _zero;
                    if (UIManager.Instance.GetPickState())
                    {
                        UIManager.Instance.SetHand(false, _zero);
                    }
                }
            }
            else
            {
                if (lastPickGun > _zero)
                {
                    lastPickGun -= Time.deltaTime;
                }
            }

            if (UIManager.Instance.GetPickState())
            {
                UIManager.Instance.SetHand(true, lastPickGun / timePickGun);
            }
        }
        else
        {
            if (UIManager.Instance.GetPickState())
            {
                UIManager.Instance.SetHand(false, _zero);
                lastPickGun = _zero;
            }
        }

        if (actualGun != null)
        {
            if (lastShootTime <= _zero && !reloadindg && !isChanging)
            {
                actualGun.gunReady = true;
                if (!actualGun.data.automatic)
                {
                    if (Input.GetButtonDown("Fire1"))
                    {
                        Shoot();
                    }
                }
                else
                {
                    if (Input.GetButton("Fire1"))
                    {
                        Shoot();
                    }
                }
            }
            else
            {
                actualGun.gunReady = false;
            }
            
            if (Input.GetKey(KeyCode.R) && !reloadindg && !isChanging)
            {
                if (actualGun.actualAmmo < actualGun.data.maxAmmoCount)
                {
                    lastReload = 0;
                    reloadindg = true;
                    
                }
            }

            if (UIManager.Instance.GetReloadState() && !reloadindg)
            {
                UIManager.Instance.SetReload(false, _zero);
            }
            
            targetRotation = Vector3.Lerp(targetRotation, Vector3.zero, actualGun.data.returnSpeed * Time.deltaTime);
            currentRotation = Vector3.Slerp(currentRotation, targetRotation, actualGun.data.snappines * Time.deltaTime);
            gunPoint.localRotation = Quaternion.Euler(currentRotation);
        }
        else
        {
            gunPoint.localRotation = Quaternion.Slerp(gunPoint.localRotation, Quaternion.identity, defaultReturnSpeed * Time.deltaTime);
        }

        if (lastShootTime >= _zero)
        {
            lastShootTime -= Time.deltaTime;
        }

        if (reloadindg)
        {
            lastReload += Time.deltaTime;
            UIManager.Instance.SetReload(true, lastReload/actualGun.ReloadTime());
            if (lastReload >= actualGun.ReloadTime())
            {
                reloadindg = false;
                Reload();
            }
        }

        if (Input.mouseScrollDelta.y != _zero && !reloadindg)
        {
            indexGun += (int)Input.mouseScrollDelta.y;
            if (indexGun >= _maxGuns)
            {
                indexGun = 0;
            }

            if (indexGun < 0)
            {
                indexGun = _maxGuns - 1;
            }

            lastChangeTime = 0;
            if (actualGun != null)
            {
                actualGun.gameObject.SetActive(false);
                actualGun = null;
            }

            isChanging = true;
        }

        if (isChanging)
        {
            lastChangeTime += Time.deltaTime;
            if (lastChangeTime >= changeTime)
            {
                isChanging = false;
                ChangeGun(indexGun);
            }
        }
    }

    #endregion

    #region Custom Functions

    private void Shoot()
    {
        lastShootTime = actualGun.data.fireRate;
        if (actualGun.actualAmmo > _zero)
        {
            actualGun.Shoot();
            AddRecoil();
        }
        else
        {
            actualGun.ShootEmpty();
        }
    }

    private void AddRecoil()
    {
        targetRotation += new Vector3(actualGun.data.recoil.x, Random.Range(-actualGun.data.recoil.y, actualGun.data.recoil.y), _zero);
    }

    private void Reload()
    {
        actualGun.actualAmmo = actualGun.data.maxAmmoCount;
        UIManager.Instance.SetReload(false, _zero);
    }

    private void ChangeGun(int index)
    {
        if (guns[index] != null)
        {
            actualGun = guns[index];
            actualGun.gameObject.SetActive(true);
            ActionsController.OnSetCamera?.Invoke(camera.transform);
        }
    }

    private void PickGun(Gun newGun)
    {
        if (guns[indexGun] != null)
        {
            DropGun(guns[indexGun]);
            if (actualGun == guns[indexGun])
            {
                actualGun = null;
            }
        }

        newGun.GetComponent<Rigidbody>().isKinematic = true;
        newGun.transform.parent = gunPoint;
        newGun.transform.localPosition = newGun.data.offsetPosition;
        newGun.transform.localRotation = Quaternion.identity;
        guns[indexGun] = newGun;
        actualGun = guns[indexGun];
        ActionsController.OnSetCamera?.Invoke(camera.transform);
    }

    private void DropGun(Gun dropGun)
    {
        dropGun.gunReady = false;
        dropGun.transform.parent = null;
        Rigidbody rb = dropGun.GetComponent<Rigidbody>();
        rb.isKinematic = false;
        rb.AddForce(dropGun.transform.up * _dropForce, ForceMode.Impulse);
        rb.AddTorque(dropGun.transform.forward * _dropForce, ForceMode.Impulse);
    }

    #endregion
}