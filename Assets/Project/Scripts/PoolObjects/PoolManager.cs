using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Unity.VisualScripting;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    public static PoolManager Instance;
    [SerializeField] private MyPool[] _pools;
    private const int _one = 1;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void CreateObjects(int quantity, int indexPool)
    {
        for (int i = 0; i < quantity; i++)
        {
            GameObject go = Instantiate(_pools[indexPool].prefObject, _pools[indexPool].objectParent).gameObject;
            go.SetActive(false);
            _pools[indexPool].objects.Add(go);
            go.name = $"{_pools[indexPool].namePool}_{go.transform.GetSiblingIndex()}";
        }
    }

    public GameObject GetObject(int indexPool)
    {
        for (int i = 0; i < _pools[indexPool].objects.Count; i++)
        {
            if (!_pools[indexPool].objects[i].activeSelf)
            {
                _pools[indexPool].objects[i].SetActive(true);
                return _pools[indexPool].objects[i];
            }
        }
        CreateObjects(_one, indexPool);
        _pools[indexPool].objects[^_one].SetActive(true);
        return _pools[indexPool].objects[^_one];
    }


    [Serializable]
    private class MyPool
    {
        public string namePool;
        public GameObject prefObject;
        public Transform objectParent;
        public List<GameObject> objects;
    }
}


