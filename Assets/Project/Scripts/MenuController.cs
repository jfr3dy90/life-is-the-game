using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField] private Button isDanceButton;
    [SerializeField] private Button[] buttons;
    [SerializeField] private Button continueButton;
    [SerializeField] private GameObject contentIsDanceButton;
    
    private bool _isDance = true;
    private const int _sceneIndex = 1;

    private void Start()
    {
        isDanceButton.onClick.AddListener(DanceController);
        continueButton.onClick.AddListener(LoadScene);
        DanceController();
    }
    
    private void OnDestroy()
    {
        isDanceButton.onClick.RemoveListener(DanceController);
    }

    private void DanceController()
    {
        _isDance = !_isDance;
        contentIsDanceButton.SetActive(_isDance);
        ActionsController.OnDance?.Invoke(_isDance);
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].interactable = _isDance;
        }
    }

    public void SelectDance(float danceValue)
    {
        ActionsController.OnSelectDance?.Invoke(danceValue);
    }

    private void LoadScene()
    {
        GameManager.Instance.LoadNextScene(_sceneIndex);
    }
}
