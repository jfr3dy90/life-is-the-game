using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHoleLauncher : Gun
{
    public override float ReloadTime()
    {
        return data.reloadTime;
    }
    
    public override void Shoot()
    {
        GameObject go = PoolManager.Instance.GetObject(poolIndex);
        go.transform.position = spawnPoint.position;
        go.transform.rotation = spawnPoint.rotation;
        base.Shoot();
    }
}
