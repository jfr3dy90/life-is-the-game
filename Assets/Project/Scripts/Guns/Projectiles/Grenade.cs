using UnityEngine;

public class Grenade : MonoBehaviour
{
    [SerializeField] private float radius;
    [SerializeField] private GameObject pref_explosion;
    [SerializeField] private float explosionForce;
    [SerializeField] private AudioClip explosionSfx;
    private void OnCollisionEnter(Collision collision)
    {
        Explode();
    }

    private void Explode()
    {
        SoundFxController.Instance.PlaySoundFx(transform.position, explosionSfx);
       // GameObject go = Instantiate(pref_explosion, transform.position, transform.rotation);
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);

        foreach (Collider col in colliders)
        {
            Rigidbody rb = col.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(explosionForce, transform.position, radius);
            }
        }
        
        gameObject.SetActive(false);
    }
}
