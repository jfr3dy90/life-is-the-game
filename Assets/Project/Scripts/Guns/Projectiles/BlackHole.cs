using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : MonoBehaviour
{
    [SerializeField] private float lifeTime;
    [SerializeField] private float movementSpeed;
    [SerializeField] private float radius;
    [SerializeField] private float forceAttraction;
    private float lastLifeTime;
    private const float _two = 2f;

    private void Update()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
        foreach (Collider col in colliders)
        {
            Rigidbody rb = col.GetComponent<Rigidbody>();
            if (rb != null)
            {
                Attract(rb);
            }
        }
        transform.Translate((movementSpeed * Time.deltaTime) * Vector3.forward);

        lastLifeTime += Time.deltaTime;
        if (lastLifeTime >= lifeTime)
        {
            lastLifeTime = 0;
            gameObject.SetActive(false);
        }

    }
    
    

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
    }

    private void Attract(Rigidbody rbToAttract)
    {
        Vector3 direction = transform.position - rbToAttract.position;
        float distance = direction.magnitude;
        float forceMagnitude = forceAttraction / Mathf.Pow(distance, _two);
        Vector3 force = direction.normalized * forceMagnitude;
        rbToAttract.AddForce(force);
    }
}
