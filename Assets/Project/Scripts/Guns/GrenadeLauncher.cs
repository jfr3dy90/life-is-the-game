using System;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeLauncher : Gun
{
    [Header("Proyection")] 
    [SerializeField] private int numPoints;
    [SerializeField] private float timeBetweenPoints;
    [SerializeField] private LayerMask layerMask;
    private LineRenderer _lr;
    
 
    public override float ReloadTime()
    {
        float reloadTime = data.reloadTime * (data.maxAmmoCount - actualAmmo);
        return reloadTime;
    }

    protected override void Start()
    {
        _lr = GetComponent<LineRenderer>();
        base.Start();
    }

    private void Update()
    {
        if (gunReady)
        {
            DrawPath();
        }
            _lr.enabled = gunReady;
    }

    private void DrawPath()
    {
        _lr.positionCount = numPoints;
        List<Vector3> points = new List<Vector3>();
        Vector3 startPosition = spawnPoint.position;
        Vector3 startVelocity = spawnPoint.forward * data.range;

        for (float t = 0; t < numPoints;  t+=timeBetweenPoints)
        {
            Vector3 newPoint = startPosition + t * startVelocity;
            newPoint.y = startPosition.y + startVelocity.y * t + Physics.gravity.y / 2f * t * t;
            points.Add(newPoint);

            if (Physics.OverlapSphere(newPoint, 2, layerMask).Length > 0)
            {
                _lr.positionCount = points.Count;
            }
        }
        _lr.SetPositions(points.ToArray());
    }

    public override void Shoot()
    {
        GameObject go = PoolManager.Instance.GetObject(poolIndex);
        go.transform.position = spawnPoint.position;
        go.transform.rotation = spawnPoint.rotation;
        go.GetComponent<Rigidbody>().velocity = spawnPoint.forward * data.range;
        base.Shoot();
    }
}
