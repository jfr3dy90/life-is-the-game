using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : Gun
{
    [SerializeField] private float forceBullet;
    [SerializeField] private GameObject impactEffect;
    private Transform camPlayer;
    private const float distanceDecal = 0.001f;
    public override float ReloadTime()
    {
        return data.reloadTime;
    }

    protected override void Start()
    {
        ActionsController.OnSetCamera += OnGetCamera;
        base.Start();
    }

    private void OnGetCamera(Transform cam)
    {
        camPlayer = cam;
    }

    private void OnDestroy()
    {
        ActionsController.OnSetCamera -= OnGetCamera;
    }

    public override void Shoot()
    {
        if (Physics.Raycast(camPlayer.position, camPlayer.forward, out RaycastHit hit))
        {
            if (hit.transform != null)
            {
                GameObject go =PoolManager.Instance.GetObject(poolIndex);
                go.transform.position = hit.point + (hit.normal * distanceDecal);
                go.transform.rotation = Quaternion.LookRotation(hit.normal, Vector3.up);
                
                go.transform.parent = null;
                go.transform.localScale = new Vector3(0.1f, 0.1f, -1f);
                go.transform.parent = hit.transform;
                
                Rigidbody rb = hit.transform.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.AddForceAtPosition(  rb.transform.TransformDirection((rb.position - hit.point).normalized)  * forceBullet, hit.point, ForceMode.Force);
                }
            }
        }
        base.Shoot();
    }
}
