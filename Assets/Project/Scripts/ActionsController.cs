using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ActionsController
{
   public static Action<bool> OnDance;
   public static Action<float> OnSelectDance;
   public static Action<Transform> OnSetCamera;
}
