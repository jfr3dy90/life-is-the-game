using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Rendering;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{
   #region Variables

   [Header("Camera Rotation")] 
   [SerializeField] private float sensitiveX;
   [SerializeField] private float sensitiveY;
   [SerializeField] private Transform pointOfView;

   private float _xRotation;
   private float _yRotation;
   private const float _verticalAngleView = 60;
   private const float _zero = 0f;
   
   
   [Header("Movement")] 
   [SerializeField] private float movementSpeed;
   [SerializeField] private float jumpHeight;

   private float _horizontal;
   private float _vertical;
   private float _verticalSpeed;
   private Vector3 _moveDirection;
   private Vector3 _verticalMove;
   private Transform _tr;
   private CharacterController _controller;
   private const float _gravity = -9.81f;
   private const float _jumpConst = -2f;

   [Header("GroundCheck")] 
   [SerializeField] private float radius;
   [SerializeField] private float checkDistance;
   [SerializeField] private Vector3 checkOffset;
   [SerializeField] private LayerMask groundLayer;

   private const float _chekRadius = 0.1f;
   #endregion

   #region UnityFunctions

   private void Start()
   {
      //Get components
      _controller = GetComponent<CharacterController>();
      _tr = GetComponent<Transform>();
      
      //Lock and hide cursor
      Cursor.lockState = CursorLockMode.Locked;
      Cursor.visible = false;
   }

   private void Update()
   {
      CameraRotation();
      Movement();
   }
   private void OnDrawGizmos()
   {
      //If the _tr value is null get the component Transform
      if (_tr == null)
      {
         _tr = GetComponent<Transform>();
      }
      
      //Draw a wire sphere in the origin of the check
      Gizmos.DrawWireSphere(_tr.position+checkOffset, radius);
      Vector3 _endPoint = new Vector3();
      if (Physics.SphereCast(_tr.position + checkOffset, radius, Vector3.down, out RaycastHit hit, checkDistance, groundLayer))
      {
         // If detect Ground draws the point of contact
         Gizmos.color = Color.green;
         _endPoint = (hit.point + Vector3.up * radius);
         Gizmos.DrawSphere(hit.point, _chekRadius);
      }
      else
      {
         // If NOT detect Ground draws the lenght of the groundChecker
         _endPoint = ((_tr.position + checkOffset) + (Vector3.down * checkDistance));
         Gizmos.color = Color.red;
      }
      
       //Draws the result if the GroundChecker
      Gizmos.DrawWireSphere(_endPoint, radius);
      Gizmos.DrawLine(_tr.position + checkOffset, _endPoint);
   }

   #endregion

   #region CustomFunctions

   private void CameraRotation()
   {
      //Get mouse input
      float mouseX = Input.GetAxisRaw("Mouse X") * Time.deltaTime * sensitiveX;
      float mouseY = Input.GetAxisRaw("Mouse Y") * Time.deltaTime * sensitiveY;
      
      //Store mouse input
      _yRotation += mouseX;
      _xRotation -= mouseY;
      
      //Limit vertical rotation for camera
      _xRotation = Mathf.Clamp(_xRotation, -_verticalAngleView, _verticalAngleView);
      
      //apply rotetion to body and head
      transform.rotation = Quaternion.Euler(_zero, _yRotation, _zero);
      pointOfView.localRotation = Quaternion.Euler(_xRotation, _zero, _zero);
   }

   private void Movement()
   {
      //Get input Axis
      _horizontal = Input.GetAxisRaw("Horizontal");
      _vertical = Input.GetAxisRaw("Vertical");
      
      if (IsGrounded() && _verticalMove.y < 0)
      {
         _verticalMove.y = _jumpConst;
      }

      _moveDirection = _tr.right * _horizontal + _tr.forward * _vertical;
      _controller.Move(_moveDirection.normalized * (movementSpeed * Time.deltaTime));

      if (IsGrounded() && Input.GetButtonDown("Jump"))
      {
         _verticalMove.y = Mathf.Sqrt(jumpHeight * _jumpConst * _gravity);
      }

      _verticalMove.y += _gravity * Time.deltaTime;
      _controller.Move(_verticalMove * Time.deltaTime);
   }

   private bool IsGrounded()
   {
      //Check if the character is on Ground
      bool isGrounded = Physics.SphereCast(_tr.position + checkOffset, radius, Vector3.down, out RaycastHit hit, checkDistance, groundLayer);
      return isGrounded;
   }
   #endregion
}
