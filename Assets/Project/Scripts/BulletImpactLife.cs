using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletImpactLife : MonoBehaviour
{
    private const float lifeTime = 5f;
    private void OnEnable()
    {
        Invoke("ReturnToPol", lifeTime);
    }

    private void ReturnToPol()
    {
        gameObject.SetActive(false);
    }
}
