using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
   #region Variables
   public static UIManager Instance;
   [SerializeField] private Image pickIndicator;
   [SerializeField] private Image reloadParent;
   [SerializeField] private Image roloadFill;
   #endregion

   #region Unity Functions

   private void Awake()
   {
      if (Instance == null)
      {
         Instance = this;  
      }
      else
      {
         Destroy(this);
      }
   }

   #endregion

   #region Custom Functions

   public void SetHand(bool state, float fill)
   {
      pickIndicator.gameObject.SetActive(state);
      FillUI(fill, pickIndicator);
   }

   public void SetReload(bool state, float fill)
   {
      reloadParent.gameObject.SetActive(state);
      FillUI(fill, roloadFill);
   }

   public bool GetPickState()
   {
      return  ImageState(pickIndicator);
   }

   public bool GetReloadState()
   {
      return ImageState(reloadParent);
   }

   private bool ImageState(Image img)
   {
      return img.gameObject.activeInHierarchy;
   }

   private void FillUI(float fill, Image img)
   {
      img.fillAmount = fill;
   }
   
   #endregion
}
